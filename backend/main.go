package main

import (
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
)

type Response struct {
	Message string `json:"message"`
}

func main() {

	port := os.Getenv("PORT")

	if port == "" {
		port = "3000"
	}

	e := echo.New()

	e.Use(middleware.CORS())

	e.GET("/", func(c echo.Context) error {
		log.Info("Get /")
		return c.JSON(200, &Response{Message: "Hello World from server"})
	})
	e.Logger.Fatal(e.Start(":" + port))
}
