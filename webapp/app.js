(()=>{
    function click() {
  
      const baseURL = 'http://localhost:3000';
      let url = baseURL + "/";
  
      request = $.ajax({
        url: url,
        type: "get",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: (data, status, jqxhr) => {
          console.log(data);
          $('#result').html(data.message);
        },
        error: (jqxhr, status, err) => {
          console.log(err)
        }
      })
    }
  
    let btn = document.getElementById("btn")
    btn.addEventListener("click", click)
  })();